\documentclass[a4paper,12pt]{memoir}
\usepackage{polyglossia}
\usepackage[inner=2cm,outer=2cm,top=7mm,bottom=7mm,includeheadfoot]{geometry}
\usepackage{fancyhdr}
\usepackage{extramarks}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{xfrac}
\usepackage{fontspec}
\usepackage{longtable}
\usepackage{tabu}
\usepackage{multirow}
\usepackage{verbatim}
\usepackage{wrapfig}
\usepackage{xparse}
\usepackage{ifthen}
\usepackage{multicol}
\usepackage{keyval}
\usepackage[math-style=TeX,bold-style=TeX]{unicode-math}
\usepackage{repeatmathop}
\usepackage{tcolorbox}
\usepackage{physics}
\usepackage{siunitx}
\usepackage{etoolbox}
\usepackage{tikz}
\usepackage{contour}
\usepackage{scalerel}
\usepackage[l3]{csvsimple}
%\usepackage[inline]{showlabels}
\usepackage{hyperref}
\tcbuselibrary{skins,breakable}
\usetikzlibrary{arrows.meta}

% New definition of square root:
% it renames \sqrt as \oldsqrt
\let\oldsqrt\sqrt
% it defines the new \sqrt in terms of the old one
\def\sqrt{\mathpalette\DHLhksqrt}
\newcommand{\nthroot}[1]{\def\DHLindex{#1}\mathpalette\DHLhksqrtnth}
\def\DHLhksqrt#1#2{%
\setbox0=\hbox{$#1\oldsqrt{#2\,}$}\dimen0=\ht0
\advance\dimen0-0.3\ht0
\setbox2=\hbox{\vrule height\ht0 depth -\dimen0}%
{\hskip-0.5pt\box0\hskip-0.15pt\lower0.65pt\box2}}
\def\DHLhksqrtnth#1#2{%
  \setbox0=\hbox{$#1\oldsqrt[\DHLindex]{#2\,}$}\dimen0=\ht0
  \advance\dimen0-0.3\ht0
  \setbox2=\hbox{\vrule height\ht0 depth -\dimen0}%
  {\hskip-0.5pt\box0\hskip-0.15pt\lower0.65pt\box2}}
\hypersetup{
	unicode=true,
	pdfstartview={FitH},
	pdftitle={Songs of Conquest info book},
	pdfnewwindow=true,
	colorlinks=true,
	linkcolor=red,
	citecolor=green,
	filecolor=magenta,
	urlcolor=cyan,
	pdfborder={0.2 0.2 0.3 [1 1]},
	bookmarksnumbered=true,
	bookmarksopen=true,
	bookmarksopenlevel=1,
	pdfpagemode=UseOutlines,
}

\sisetup{
	exponent-product=\cdot,
	output-decimal-marker={,},
	angle-symbol-over-decimal,
	group-digits=true,
	group-separator=\text{~},
	group-minimum-digits=4,
	detect-all,
}

\setmainfont{Noah Grotesque}[
	Ligatures = TeX,
	ItalicFeatures  = {Style = Swash},
	SmallCapsFeatures={Ligatures = NoCommon},
]

\setmonofont{monofur}
\setmainlanguage{english}

\counterwithout{section}{chapter}
\setcounter{tocdepth}{2}

\begin{document}

\newcommand\scaps{\addfontfeature{Letters=SmallCaps}}
\setsecheadstyle{\fontspec[Scale=1.2,Ligatures=TeX]{Glot}\LARGE\bfseries\centering\MakeUppercase}
\setsubsecheadstyle{\fontspec[Scale=1.2,Ligatures=TeX]{Glot}\Large\scshape\centering\MakeUppercase}
\setparaheadstyle{\fontspec[Ligatures=TeX]{Glot}\MakeUppercase}
%\setsecnumformat{§\ \ \csname the#1\endcsname\quad}
\setsecnumformat{}

\newcommand{\cara}{\rule{\linewidth}{0.4mm}}
\renewcommand{\labelitemi}{$\star$}

\def\hhline{\tabucline[1.3pt]{-}}

\renewcommand{\thechapter}{\Roman{chapter}}
\chapterstyle{veelo}
\renewcommand{\chaptitlefont}{\fontspec[Scale=1.7]{Glot}\Huge\bfseries\scaps\MakeUppercase}
\renewcommand{\chapnumfont}{\fontspec{Glot}}

\renewcommand*{\printchapternum}{%
\hspace{.8em}%
\resizebox{!}{\beforechapskip}{\chapnumfont \thechapter}%
\hspace{.4em}%
\makebox[0pt][l]{
\rule{3\midchapskip}{\beforechapskip}%
}}%

\pagestyle{ruled}

\setlength{\parindent}{0mm}
\setlength{\parskip}{1mm}

\newenvironment{cd}{
\begin{description}
\setlength{\itemsep}{0.1cm}
\setlength{\parskip}{0cm}
\setlength{\parsep}{0cm}}{\end{description}}
\newenvironment{ce}{
\begin{enumerate}
\setlength{\itemsep}{0.1cm}
\setlength{\parskip}{0cm}
\setlength{\parsep}{0cm}}{\end{enumerate}}
\newenvironment{ci}{
\begin{itemize}
\setlength{\itemsep}{0.1cm}
\setlength{\parskip}{0cm}
\setlength{\parsep}{0cm}}{\end{itemize}}
\setlength{\intextsep}{0cm}

\newcommand\nejasne[1]{{\bfseries\color{red} #1}}

\newcommand\resourceGold{\scalerel*{\includegraphics{images/icons/resources/gold.png}}{\strut}}
\newcommand\resourceWood{\scalerel*{\includegraphics{images/icons/resources/wood.png}}{\strut}}
\newcommand\resourceStone{\scalerel*{\includegraphics{images/icons/resources/stone.png}}{\strut}}
\newcommand\resourceAncientAmber{\scalerel*{\includegraphics{images/icons/resources/amber.png}}{\strut}}
\newcommand\resourceGlimmerweave{\scalerel*{\includegraphics{images/icons/resources/glimmerweave.png}}{\strut}}
\newcommand\resourceCelestialOre{\hspace{2pt}\scalerel*{\includegraphics{images/icons/resources/celestial-ore.png}}{\strut}}

\newcommand{\essenceball}[2]{\raisebox{-1.5mm}{\begin{tikzpicture} \shade[shading=ball,ball color=#1] (0,0) circle (2.2mm); \node at (0,0) {\color{white} \contour{black}{\bfseries #2}}; \end{tikzpicture}}}

\newcommand{\essenceorder}[1]{\essenceball{blue}{#1}}
\newcommand{\essencecreation}[1]{\essenceball{orange}{#1}}
\newcommand{\essencechaos}[1]{\essenceball{red!50!blue}{#1}}
\newcommand{\essencedestruction}[1]{\essenceball{red}{#1}}
\newcommand{\essencearcana}[1]{\essenceball{blue!50!green}{#1}}

\newcommand\unitStatsTable[1]{{\small\centering
\setlength{\tabcolsep}{2pt}
\begin{longtabu}{c |  ccc cc cc cccc}
	\hhline
	& & & \textbf{Stack} & & & & & & \multicolumn{2}{c}{\textbf{Offense}} & \\
	\textbf{Unit Name} & \textbf{Cost} & \textbf{Essence} & \textbf{Size} & \textbf{Init} & \textbf{Move} & \textbf{HP} & \textbf{Defense} & \textbf{Damage} & {\footnotesize \textbf{Melee}} & {\footnotesize \textbf{Ranged}} & \textbf{Ranges} \\\hline\hline
	\endhead

	\csvreader[head to column names,separator=tab,late after last line=\\\hhline]{data/units-#1.csv}{}{
		\ifcsvstrcmp{\thecsvrow}{1}{}{\\\ifcsvstrcmp{\addHline}{yes}{\hline}{}} \multirow{2}{*}{\unitName} & \unitCost & \unitEssence & \unitStackSize & \unitInit & \unitMovement & \unitHP & \unitDef & \unitDamage & \unitMelee & \ifcsvstrcmp{\unitRangeOff}{0}{}{\unitRangeOff} & \ifcsvstrcmp{\unitRangeOff}{0}{}{\unitRange} \\
		& \multicolumn{11}{l}{\textbf{Specials:} \unitSpecials} %\ifcsvstrcmp{\addHline}{normal}{\\\hline}{\ifcsvstrcmp{\addHline}{thick}{\\\hhline}{\\}}
	}
\end{longtabu}

}}

\renewcommand*{\contentsname}{Table of Contents}

\tableofcontents

\chapter[Units][Units]{UNITS}

\section{Unit statistics}

\subsection{Arleon}

\unitStatsTable{arleon}

\subsection{Barya}

\unitStatsTable{barya}

\subsection{Rana}

\unitStatsTable{rana}

\subsection{Loth}

\unitStatsTable{loth}

\subsection{Neutral Units}

\unitStatsTable{neutral}

\section{List of unit specials}

The unit specials fall into two broad categories: \emph{abilities} and everything else --- let's call that \emph{passive traits}. There are two important differences between them:
\begin{ci}
	\item Each unit can have at most one \emph{ability}, and any number of \emph{passive traits}.
	\item \emph{Passive traits} work all the time, but \emph{abilities} must be used actively during the unit's turn. 
\end{ci}
Any unit whose turn it currently is can use its \emph{ability} (if it has one). That costs 1 movement point and ends its turn (in a sense, using an \emph{ability} replaces the normal attack).

All special effects on units stack, i. e. an unit can be affected by multiple copies of the same effect. For instance, if an unit hears one song (like ``Fear no Foe'' or ``Jig of Jugs'') multiple times, it will also obtain the bonus an equal number of times.

\subsection{Abilities}

\paragraph{Aim} \label{unit-specials:Aim} \nejasne{VYJASNIT, JAK DLOUHO TO TRVÁ.}
\paragraph{Ambush} \label{unit-specials:Overwatch} This unit (let's call it the \emph{ambusher}) will wait for an enemy unit to enter a hex within the ambusher's Deadly Range. As soon as that happens, the moving unit's turn is interrupted and the ambusher performs an attack against it. After such an attack, or at the start of the ambusher's next turn, the ambush is cancelled.
\paragraph{``Aurelia Eternal’’} \label{unit-specials:AureliaEternal} Gives +5 Melee Offense, Ranged Offense and Defense to all friendly units. \nejasne{NA JAK DLOUHO?}
\paragraph{Charge Essence} \label{unit-specials:ChargeEssence} This unit's Essence is (again) added to the essences of its Wielder.
\paragraph{Defend} \label{unit-specials:Defend} This unit gains +30 Defense until the start of its next turn.
\paragraph{Faey Fire} \label{unit-specials:FaeyFire} \nejasne{JAK TO PŘESNĚ FUNGUJE??}
\paragraph{``Fear No Foe’’} \label{unit-specials:FearNoFoe} Gives +5 Defense to all friendly units. \nejasne{NA JAK DLOUHO?}
\paragraph{``For the Coin in my Hand’’} \label{unit-specials:TheCoinInMyHand} Gives +10 Melee and Ranged Offense to all friendly units. \nejasne{NA JAK DLOUHO?}
\paragraph{Oathbound Sacrifice} \label{unit-specials:SacrificeBones} \nejasne{JAK TO PŘESNĚ FUNGUJE??}
\paragraph{``Of Glories Gone’’} \label{unit-specials:HerAimWasTrue} Gives +5 Melee and Ranged Offense to all friendly units \nejasne{NA JAK DLOUHO?}
\paragraph{Place Mine} \label{unit-specials:PlaceMine} A mine is placed in front of this unit. The next unit that enters this hex will be hit for 10×\emph{(number of units in the stack that placed the mine)} damage. \nejasne{OTESTOVAT!}
\paragraph{Protect} \label{unit-specials:Protect} Gives +25 Defense to all adjacent friendly units. \nejasne{NA JAK DLOUHO?}
\paragraph{Spearwall} \label{unit-specials:Spearwall} This unit (let's call it the \emph{ambusher}) will wait for an enemy unit to enter a hex within the ambusher's Melee Range. As soon as that happens, the moving unit's turn is interrupted and the ambusher performs an attack against it. After such an attack, or at the start of the ambusher's next turn, the spearwall is cancelled.
\paragraph{Stakes} \label{unit-specials:Palisades} Stakes are placed in front of this unit. Stakes are an impassable obstacle that must be destroyed before it is possible to enter its hex. The stakes need to take \emph{(number of units in the stack that placed the stakes)} damage to be destroyed.
\paragraph{``Stoutheart Shall Stand’’} \label{unit-specials:StoutheartShallStand} Gives +10 Defense to all friendly units. \nejasne{NA JAK DLOUHO?}
\paragraph{Strengthen} \label{unit-specials:Inspire} Gives +20 Melee and Ranged Offense and +1 Damage to all adjacent friendly units. \nejasne{NA JAK DLOUHO?}
\paragraph{The Dragon's Roar} \label{unit-specials:TheDragonsRoar} Gives −10 Defense and −5 Initiative to all enemy units. \nejasne{NA JAK DLOUHO?}
\paragraph{``The Jig of Jugs’’} \label{unit-specials:JigOfJugs} Gives +5 Initiative to all friendly units. \nejasne{NA JAK DLOUHO?}
\paragraph{Wait} \label{unit-specials:Wait} Postpones the turn of this unit to the very end of the round. \nejasne{CO KDYŽ ČEKÁ VÍC JEDNOTEK? V JAKÉM HRAJÍ POŘADÍ?}

\subsection{Passive traits}
\paragraph{Backstabber} \label{unit-specials:Backstabber} Units attacked by this unit cannot retaliate.
\paragraph{Barrage} \label{unit-specials:BarrageAttack} Ranged attacks of this unit hit the target as well as all adjacent hexes.
\paragraph{Berserker} \label{unit-specials:Berserker} As soon as this unit takes any damage, it gains +1 Movement, +2 Damage, −25 Defense. \nejasne{NA JAK DLOUHO?}
\paragraph{Charger} \label{unit-specials:Charger} This unit's attacks gain +10 Melee Offense for each hex that this unit moved through in the same turn.
\paragraph{Double Strike} \label{unit-specials:DoubleStrike} When attacking, the unit performs two attacks on the same target. The second attack is made after the target's retaliation (if any). If the target is killed with the first strike, the second attack can be used against another target, but the unit may not move or use abilities in between. This trait has no effect on this unit's retaliations.
\paragraph{Flame Attacks} \label{unit-specials:FlameAttack} \nejasne{ZJISTIT, CO PŘESNĚ TO DĚLÁ.}
\paragraph{Guard} \label{unit-specials:Guard} All adjacent friendly units gain +10 Defense. \nejasne{ZJISTIT, JAK DLOUHO TO FUNGUJE.}
\paragraph{Helpless} \label{unit-specials:Helpless} This unit cannot retaliate.
\paragraph{Inspiring} \label{unit-specials:Inspiring} All adjacent friendly units gain +5 Melee and Ranged Offense and +5 Initiative. \nejasne{ZJISTIT, JAK DLOUHO TO FUNGUJE.}
\paragraph{Intimidating} \label{unit-specials:Intimidating} All adjacent enemy units suffer −10 Defense and −10 Initiative. \nejasne{ZJISTIT, JAK DLOUHO TO FUNGUJE.}
\paragraph{Magic Resistance} \label{unit-specials:MagicResistance} \nejasne{TAHLE SCHOPNOST VŮBEC NEEXISTUJE, NEBO CO?!?!?}
\paragraph{Persistent} \label{unit-specials:Persistent} This unit has (practically) unlimited retaliations.
\paragraph{Quick} \label{unit-specials:Quick} This unit retaliates \emph{before} the attacking unit strikes.
\paragraph{Reach} \label{unit-specials:Reach} This unit can use melee attacks against enemies 2 hexes away.
\paragraph{Reload} \label{unit-specials:Reload} After each ranged attack, this unit must spend one turn reloading. During reloading, the unit cannot attack (melee or ranged) but it may move, use abilities and retaliate in melee.
\paragraph{Shielded} \label{unit-specials:Shielded} This unit has 50\% Ranged Resistance (meaning that ranged attacks do only half damage to it).
\paragraph{Stealthy} \label{unit-specials:Stealthy1} This unit can move through enemies' zones of control without provoking attacks of opportunity.
\paragraph{Sweeping Attacks} \label{unit-specials:SweepAttack} \nejasne{ZJISTIT, CO PŘESNĚ TO DĚLÁ.}
\paragraph{Venomous} \label{unit-specials:Venomous} \nejasne{ZJISTIT, CO PŘESNĚ TO DĚLÁ.}
\end{document}
